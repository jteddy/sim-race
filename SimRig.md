# Sim Racing Rig

## Hand Brake

| Name                                                         | Price                                                        | Points                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------------------------------------------- |
| [Heusinkveld Sim Handbrake](https://heusinkveld.com/products/shifters-handbrakes/sim-handbrake-2/?v=6cc98ba2045f) | €[197.52](https://heusinkveld.com/products/shifters-handbrakes/sim-handbrake-2/)<br>$[309](https://hrsims.com/collections/handbrakes/products/sim-handbrake) AUD | - Tilt 120 degrees <br>- Forces up to 120kg<br/>- Loadcell |
| [Aiologs Handbrake](http://shop.aiologs.net/handbrake.html)  | $165 USD<br>$243.66 AUD                                      | - Forces up to 4kg                                         |
| [Thrustmaster TSS](https://www.thrustmaster.com/en_US/products/tss-handbrake-sparco-mod) | $[399](https://www.mwave.com.au/product/thrustmaster-tss-handbrake-sparco-mod-for-pc-xbox-one-ps4-ac20099?gclid=CjwKCAjw7anqBRALEiwAgvGgmw1xiGWAgOPxzgO4FOLL8-7_LwaNNOk1yF2ekb_l65jtXEc6uCYTABoCHQQQAvD_BwE) AUD |                                                            |
| [Fantatec ClubSport Handbrake V1.5](https://www.fanatec.com/eu-en/sim-racing-hardware/clubsport-handbrake-v1-5.html) | $[199](https://www.fanatec.com/au-en/product/pc#simracinghardware) AUD | - Console compatible<br>                                   |
| [DSD Type 3 Hydraulic Handbrake](http://derekspearedesigns.com/handbrake.html) | $595 USD                                                     |                                                            |

A review of the available Sim handbrakes on the market you find the top tier products using load cells like the Heusinkveld or a mechanical mechanism like Aiologs. The Aiologs handbrake wins for looks from myself  and the handle can be extended although the DSD Type 3 hydraulic handbrake has to win for is authenticity to a real car. With the $880 price tag I believe a DIY solution can net you the same result with a few extra dollars left in your pocket.

Estimated build cost is $450 AUD. That's 65% cheaper than the DSD.

### DIY Solution

The goal is to replicate [DSD Type 3 Hydraulic Handbrake](http://derekspearedesigns.com/handbrake.html) which has been done before. [JoshMullen35 Youtube - How To Build Real USB Hydraulic Handbrake - Sim Racing](https://www.youtube.com/channel/UCVBI1H-yMEGEjd8yty1_euA) is the one of the best instructional videos I have found at the time of this writing.

![](http://derekspearedesigns.com/uploads/3/5/2/8/35282814/6091667_orig.jpg)

#### Parts

##### Hydraulic Handbrake

|                                                              |            |                                                              |
| ------------------------------------------------------------ | ---------- | ------------------------------------------------------------ |
| [VR RACING - Universal Jdm Hydraulic Horizontal Rally Drifting E-brake Lever HandBrake VR3633](https://www.aliexpress.com/item/32746440037.html?spm=a2g0o.productlist.0.0.5635f0042sJKaX&algo_pvid=2ed17b8c-b53d-4e62-a449-60e72bf3f119&algo_expid=2ed17b8c-b53d-4e62-a449-60e72bf3f119-12&btsid=35374cf3-9fda-46d8-a75d-eec762f489bf&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60) | $78.29 AUD | ![](https://ae01.alicdn.com/kf/HTB1axUroN9YBuNjy0Ffq6xIsVXa6/VR-RACING-Universal-Jdm-Hydraulic-Horizontal-Rally-Drifting-E-brake-Lever-HandBrake-VR3633.jpg_50x50.jpg) |
| [Universal Jdm Hydraulic Horizontal Rally Drifting E-brake Lever HandBrake VR3633](https://www.aliexpress.com/item/32973747215.html?spm=a2g0o.productlist.0.0.104a4f0aJP2fTj&algo_pvid=b367b388-1888-4cb8-a264-1ffd819aed42&algo_expid=b367b388-1888-4cb8-a264-1ffd819aed42-1&btsid=abbda1f7-aca4-4a1a-8519-42e2d383799d&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60) | $78.20 AUD |                                                              |
| [Universal Racing Car Black Handbrake Neo chrome Aluminum Oil Hydraulic Handbrake Lever Drift Hand brake E-Brake Type-10](https://www.aliexpress.com/item/32968883226.html?spm=a2g0o.productlist.0.0.5635f0042sJKaX&algo_pvid=99571933-6335-4e90-9962-8da153303e90&algo_expid=99571933-6335-4e90-9962-8da153303e90-2&btsid=1496337f-fa68-4c11-bb53-7873368e0a10&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60) | $83.62     | ![](https://ae01.alicdn.com/kf/HTB11bMkasnrK1RkHFrdq6xCoFXaU/Universal-Racing-Car-Black-Handbrake-Neo-chrome-Aluminum-Oil-Hydraulic-Handbrake-Lever-Drift-Hand-brake-E.jpg_50x50.jpg) |
| [Aluminum Alloy Multiple Colour Hydraulic Handbrake Universal Oil Tank For Drift Rally Racing Aluminum alloy Precision #P10](https://www.aliexpress.com/item/4000050174817.html?spm=a2g0o.productlist.0.0.5635f0042sJKaX&algo_pvid=2ed17b8c-b53d-4e62-a449-60e72bf3f119&algo_expid=2ed17b8c-b53d-4e62-a449-60e72bf3f119-42&btsid=35374cf3-9fda-46d8-a75d-eec762f489bf&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60) | $71.66     |                                                              |
| [Adjustable Aluminum Vertical Hydraulic Drifting Hand Brake With Special Master Cylinder S14 S13 For BMW F20 EP-B88008-BK](https://www.aliexpress.com/item/32432989014.html?spm=a2g0o.productlist.0.0.5635f0042sJKaX&algo_pvid=a2e2b5ca-ee03-4e3f-bb41-b6a964ec3e0f&algo_expid=a2e2b5ca-ee03-4e3f-bb41-b6a964ec3e0f-24&btsid=bba0bd0f-84a1-425e-b0af-42cbbeb3670c&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60) | $78.05     |                                                              |
| [Adjustable Aluminum Vertical Hydraulic Drifting Hand Brake With Special Master Cylinder S14 S13 For BMW F20 K8-11008](https://www.aliexpress.com/item/4000043114872.html?spm=a2g0o.productlist.0.0.5635f0042sJKaX&algo_pvid=a2e2b5ca-ee03-4e3f-bb41-b6a964ec3e0f&algo_expid=a2e2b5ca-ee03-4e3f-bb41-b6a964ec3e0f-29&btsid=bba0bd0f-84a1-425e-b0af-42cbbeb3670c&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60) | $78.21     |                                                              |

https://www.aliexpress.com/item/32973747215.html?spm=a2g0o.productlist.0.0.104a4f0aJP2fTj&algo_pvid=b367b388-1888-4cb8-a264-1ffd819aed42&algo_expid=b367b388-1888-4cb8-a264-1ffd819aed42-1&btsid=abbda1f7-aca4-4a1a-8519-42e2d383799d&ws_ab_test=searchweb0_0,searchweb201602_6,searchweb201603_60

Options

- E brake system type: Reservoir

   - Master cylinder size: 0.750" (3/4") 
- Hardware options: Reservoir -3AN Adapter 

##### Wilwood Pull-Type Slave Cylinder 260-1333

- [165](https://www.ianboettcherraceparts.com.au/shop/Wilwood-260-1333.html) AUD
- [$161.40](https://www.enginemaster.com.au/wilwood-alloy-clutch-slave-cylinder-wb260-1333-bla) AUD
- [$130](https://www.ebay.com.au/itm/Wilwood-Pull-Type-Clutch-Slave-Cylinder-WB260-1333-/253928327158) AUD


US Stores

- $[120](https://www.speedwaymotors.com/Wilwood-260-1333-Slave-Cylinder,63500.html?gclid=CjwKCAjw7anqBRALEiwAgvGgm_SaADvGfL9rHLrfZSpuCGwBpH8UcvbmGgV3TKPX2IM9G4fgQqCG2xoC6CcQAvD_BwE) AUD
- Amazon Includes Shipping to Aus $[143.75](https://www.amazon.com/Wilwood-260-1333-Clutch-Slave-Cylinder/dp/B002G35NV0) AUD

##### 11" 3an Stainless Steel Braided Line 

States

- [Earl's Performance 11" 3an Stainless Steel Braided Line](https://www.summitracing.com/int/parts/ear-63010111erl/overview/)  $25 AUD

##### Fragola 3an - 1/8" NPT - 3an T-fitting 

http://www.vpw.com.au/Category/Index/22234 $8.54 AUD

States

- [Fragola 3an - 1/8" NPT - 3an T-fitting](https://www.summitracing.com/int/parts/fra-599903/overview/) - $19.20 AUD
- https://www.speedwaymotors.com/Fragola-482503-BL-Tee-Adapter-Fitting-3AN-to-1-8-Inch-NPT,240554.html

##### 1000 PSI Pressure sensor

Must have 3 wires.

- 5V Fuel Pressure Transducer Sender 1000 PSI - eBay $[20.56](https://www.ebay.com.au/itm/Vacuum-Pressure-Transducer-Sender-Stainless-Steel-4-5V-For-Oil-Fuel-Air-Water-AU/152618289939?hash=item2388c24b13:m:mkbT47EiQ9Ng9MWtuZIDGEg&frcectupt=true)
- Aliexpress - $[24.32](https://www.aliexpress.com/item/32825224874.html)

##### Fragola 3an Female 90 Degree Fitting 


States

- $14 USD https://www.summitracing.com/parts/fra-496303-bl

#####  Earl's Performance 3/8-24 INV to -3AN Fitting 

States

- $10.15 USD https://www.summitracing.com/parts/ear-581531erl
- 

##### RipTide Longboard Bushings 


States

- $8.50 https://www.riptidesports.com/aps-chubby-duro-choices/

##### DSD 12-Bit Controller - STANDARD

$40 USD Free shipping world wide 

- http://derekspearedesigns.com/12-bit-controllers.html

Other boards?

https://www.adafruit.com/product/1085?gclid=CPuGwOr778wCFYZefgodrn0A7Q

##### Plastic Enclosure 



##### 3-Pin Female Arduino Cable 





#### Sources

- [JoshMullen35 Youtube - How To Build Real USB Hydraulic Handbrake - Sim Racing](https://youtu.be/gpbOFD2x1p0)
- [ISRTV - DSD Hydraulic Handbrake DIY Project](https://www.isrtv.com/forums/topic/10762-dsd-hydraulic-handbrake-diy-project/)



Optional

Drivehub - $89.99 USD

https://collectiveminds.ca/shop/allconsoles/drivehub/

## Sequential Shifter

- [2015 DSD Pro Sequential Shifter](http://derekspearedesigns.com/2015-dsd-pro-sequential.html)

## Seat

Sparco Sprint - $450

Sparco R100 - $500

official Sparco duel locking seat slide

